package com.example.springbootlogrequestid.holder;

import com.example.springbootlogrequestid.dto.LogDTO;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * @author will.tuo
 * @date 2023/3/9 17:06
 * @description 无
 */
public class JoinPointLogConverter {

    /**
     * 将方法的参数列表，根据参数名称和参数数据转换成{@link LogDTO}中的属性与字段
     */
    public static LogDTO getLogDTO(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Parameter[] parameters = method.getParameters();
        LogDTO logDTO = new LogDTO();
        if (parameters == null || parameters.length == 0) {
            return logDTO;
        }
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < parameters.length; i++) {
            logDTO.put(parameters[i].getName(), args[i]);
        }
        return logDTO;
    }
}
