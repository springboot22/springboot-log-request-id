package com.example.springbootlogrequestid.holder;

import com.example.springbootlogrequestid.annontation.GsonLog;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author will.tuo
 * @date 2023/3/9 15:18
 * @description 无
 */
public class GsonFactory {

    public static Gson createDefaultLogGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(
                new ExclusionStrategy() {
                    /**
                     * 只有加了 {@link GsonLog} 注解的属性才会被序列化
                     */
                    @Override
                    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                        return fieldAttributes.getAnnotation(GsonLog.class) == null;
                    }

                    /**
                     * 这里写的是看类是否要跳过，但他会在判断了DTO类的限制后又去判断属性的类，很奇怪
                     * 所以这里干脆就直接不跳过，只判断属性上的 {@link GsonLog} 注解
                     */
                    @Override
                    public boolean shouldSkipClass(Class<?> aClass) {
                        return false;
                    }
                }
        );
        return gsonBuilder.create();
    }
}
