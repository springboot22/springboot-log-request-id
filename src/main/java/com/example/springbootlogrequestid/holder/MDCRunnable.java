package com.example.springbootlogrequestid.holder;

import java.util.Map;
import org.slf4j.MDC;

/**
 * @author will.tuo
 * @date 2023/3/9 14:21
 * @description 通过装饰器模式，将父线程的MDC信息装配到子线程中
 */
public class MDCRunnable implements Runnable {

    private final Runnable runnable;
    private final Map<String, String> mdcContextMap;

    private MDCRunnable(Runnable runnable) {
        this.runnable = runnable;
        this.mdcContextMap = MDC.getCopyOfContextMap();
    }

    private void prepareMDC() {
        MDC.setContextMap(this.mdcContextMap);
    }

    private void destroyMDC() {
        mdcContextMap.clear();
    }

    public static MDCRunnable getRunnableInstance(Runnable runnable) {
        return new MDCRunnable(runnable);
    }

    @Override
    public void run() {
        prepareMDC();
        runnable.run();
        destroyMDC();
    }
}
