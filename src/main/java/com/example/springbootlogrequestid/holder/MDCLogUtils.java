package com.example.springbootlogrequestid.holder;

import java.util.UUID;
import org.slf4j.MDC;

/**
 * @author will.tuo
 * @date 2023/3/9 13:34
 * @description 无
 */
public class MDCLogUtils {

    private static final String REQUEST_ID = "requestId";

    public static void setRequestId() {
        MDC.put(REQUEST_ID, UUID.randomUUID().toString());
    }

    public static void clear() {
        MDC.clear();
    }
}
