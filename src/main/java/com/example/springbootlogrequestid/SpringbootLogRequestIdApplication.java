package com.example.springbootlogrequestid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLogRequestIdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLogRequestIdApplication.class, args);
    }

}
