package com.example.springbootlogrequestid.aspect;

import com.example.springbootlogrequestid.holder.GsonFactory;
import com.example.springbootlogrequestid.holder.JoinPointLogConverter;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author will.tuo
 * @date 2023/3/9 15:13
 * @description 无
 */
@Aspect
@Component
@Slf4j
public class ApiWebTimerAspect {

    private static Gson gson = GsonFactory.createDefaultLogGson();

    @Pointcut("execution(* com.example.springbootlogrequestid.controller.*.*(..))")
    public void webLog() {
    }

    @Around("webLog()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String requestURI = request.getRequestURI();
        request.getRemoteHost();
        long startTime = System.currentTimeMillis();
        Object proceed = pjp.proceed();
        long endTime = System.currentTimeMillis();
        long costTime = endTime - startTime;
        log.info("api:[{}],costTime:[{}],data:[{}]", requestURI, costTime,
                gson.toJson(JoinPointLogConverter.getLogDTO(pjp)));
        return proceed;
    }
}
