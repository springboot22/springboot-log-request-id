package com.example.springbootlogrequestid.aspect;

import com.example.springbootlogrequestid.holder.MDCLogUtils;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2023/3/9 13:26
 * @description MDC requestId 装入过滤器,过滤器会在切面之前生效
 */
@Order(1)
@Component
public class MDCLogFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        MDCLogUtils.setRequestId();
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        MDCLogUtils.clear();
    }
}
