package com.example.springbootlogrequestid.aspect;

import com.example.springbootlogrequestid.holder.MDCLogUtils;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author will.tuo
 * @date 2023/3/9 12:15
 * @description MDC requestId 装入切面
 */
@Aspect
@Component
public class ApiWebMDCLogAspect {

    @Pointcut("execution(* com.example.springbootlogrequestid.controller.*.*(..))")
    public void webLog() {
    }

    @Around("webLog()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String requestURI = request.getRequestURI();
        request.getRemoteHost();
        
        MDCLogUtils.setRequestId();

        Object proceed = pjp.proceed();
        MDCLogUtils.clear();
        return proceed;
    }

}
