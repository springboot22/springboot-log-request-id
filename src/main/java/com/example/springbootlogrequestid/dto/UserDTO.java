package com.example.springbootlogrequestid.dto;

import com.example.springbootlogrequestid.annontation.GsonLog;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2023/3/9 15:22
 * @description 无
 */
@GsonLog
@Data
public class UserDTO {
    
    private Long id;
    @GsonLog
    private String name;
}
