package com.example.springbootlogrequestid.controller;

import com.example.springbootlogrequestid.dto.UserDTO;
import com.example.springbootlogrequestid.holder.MDCRunnable;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2023/3/9 12:13
 * @description 无
 */
@RestController
@Slf4j
public class Controller {

    @GetMapping("/api/ping/v1")
    public Object ping() {
        System.out.println(MDC.get("requestId"));
        log.info("ping");
        new Thread(MDCRunnable.getRunnableInstance(() -> {
            log.info("ping2");
        })).start();
        return "success";
    }

    @PostMapping("/api/ping/v2")
    public Object gsonLogTest(@RequestBody UserDTO userDTO,
            @RequestParam(name = "id", defaultValue = "asfdsadf") String id) {
        return "success";
    }
}
