# MDC使用

1、通过`MDCLogUtils`封装requestId的设置

2、提供了两种方式设置

`ApiWebMDCLogAspect` controller切面

`MDCLogFilter` 过滤器

3、通过装饰器模式将runnable接口进行增强。

通过将父线程的MDC数据进行缓存在调用run方法时再进行设置。保证了子线程能够获取到父线程的MDC上下文



# 请求日志按需打印

将接口的请求数据按照 方法参数名和实际传参值，组装成json对象，进行打印。

```java
public class JoinPointLogConverter {

    /**
     * 将方法的参数列表，根据参数名称和参数数据转换成{@link LogDTO}中的属性与字段
     */
    public static LogDTO getLogDTO(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Parameter[] parameters = method.getParameters();
        LogDTO logDTO = new LogDTO();
        if (parameters == null || parameters.length == 0) {
            return logDTO;
        }
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < parameters.length; i++) {
            logDTO.put(parameters[i].getName(), args[i]);
        }
        return logDTO;
    }
}
```

将数据的字段上加上 `@GsonLog`,标识该字段需要打印到日志中，使用了Gson中的这个：

```java
public class GsonFactory {

    public static Gson createDefaultLogGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(
                new ExclusionStrategy() {
                    /**
                     * 只有加了 {@link GsonLog} 注解的属性才会被序列化
                     */
                    @Override
                    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                        return fieldAttributes.getAnnotation(GsonLog.class) == null;
                    }

                    /**
                     * 这里写的是看类是否要跳过，但他会在判断了DTO类的限制后又去判断属性的类，很奇怪
                     * 所以这里干脆就直接不跳过，只判断属性上的 {@link GsonLog} 注解
                     */
                    @Override
                    public boolean shouldSkipClass(Class<?> aClass) {
                        return false;
                    }
                }
        );
        return gsonBuilder.create();
    }
}
```

